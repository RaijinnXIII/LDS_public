from utils.auth_helpers import check_for_auth_file, check_for_env_vars
import requests
from akamai.edgegrid import EdgeGridAuth, EdgeRc
from urlparse import urljoin


class Authentication(EdgeGridAuth):
    """ Returns authorized session or raises errors """
    def __init__(self):
        """CLIENT

        CLIENT.session
        CLIENT.BASE_URL
        """
        self.session = None
        self.BASE_URL = None

        try:
            self._from_file()
        except:
            self._from_environment()


    def _from_file(self):
        creds_file = EdgeRc('.edgerc')
        try:
            if check_for_auth_file() == True:
                self.BASE_URL = 'https://%s' % creds_file.get('default', 'host')
                session = requests.Session()
                session.auth = EdgeGridAuth.from_edgerc(creds_file, 'default')
                self.session = session
            else:
                return False
        except:
            raise IOError('Could not retrieve credentials from .edgerc')

    def _from_environment(self):
        try:
            if check_for_env_vars() == True:
                self.BASE_URL = 'https://%s' % os.environment.get('EDGE_BASE_URL')
                session = requests.Session()
                session.auth = EdgeGridAuth(
                    client_token='%s' % os.environ.get('EDGE_CLIENT_TOKEN'),
                    client_secret='%s' % os.environ.get('EDGE_CLIENT_SECRET'),
                    access_token='%s' % os.environ.get('EDGE_ACCESS_TOKEN')
                )
                self.session = session
            else:
                return False
        except:
            raise SystemError('Could not retrieve credentials from environment variables')
