from authentication import Authentication
from utils.source_helpers import source_type_validation
from utils.delivery_helpers import delivery_type_validation
import json


class LogEncodings:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-configuration-parameters/'
            self.url = self.client.BASE_URL + self.endpoint

    def list(self):
        url = self.url + '/encodings'
        return self.client.session.get(url)

    def list_by_type(self, log_source_type, delivery_type):
        if source_type_validation(log_source_type) == True:
            if delivery_type_validation(delivery_type) == True:
                url = self.url + '/encodings?deliveryType=%s&logSourceType=%s' % \
                    (delivery_type, log_source_type)
                return self.client.session.get(url)

    def retrieve(self, encoding_id):
        url = self.url + '/encodings/%s' % str(encoding_id)
        return self.client.session.get(url)
