from authentication import Authentication


class LogDeliveryThresholds:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-configuration-parameters/delivery-thresholds'
            self.url = self.client.BASE_URL + self.endpoint

    def list(self):
        return self.client.session.get(self.url)

    def retrieve(self, threshold_id):
        if threshold_id == None:
            raise ValueError('threshold_id is required')

        url = self.url + '/%s' % str(threshold_id)
        return self.client.session.get(url)
