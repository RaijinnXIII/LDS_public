from authentication import Authentication


class LogContacts:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-configuration-parameters/contacts'
            self.url = self.client.BASE_URL + self.endpoint

    def list(self):
        return self.client.session.get(self.url)

    def retrieve(self, contact_id):
        if contact_id == None:
            raise ValueError('contact_id is required')

        url = self.url + '/%s' % str(contact_id)
        return self.client.session.get(url)
