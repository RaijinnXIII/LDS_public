from authentication import Authentication


class LogMessageSizes:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-configuration-parameters/message-sizes'
            self.url = self.client.BASE_URL + self.endpoint

    def list(self):
        """
        Usage: List all available log-configuration Message Sizes

        Returns HTTP response
        """
        return self.client.session.get(self.url)

    def retrieve(self, value_id):
        """
        Usage: Retrieve Message Size value by ID

        Params:
        value_id (str)

        Returns HTTP reponse
        """
        url = self.url + '/%s' % str(value_id)
        return self.client.session.get(url)
