import os, sys


def check_for_auth_file():
    """ Check for .edgerc credential file => Accept string; return __with_file() function """
    try:
        is_file = os.path.isfile('.edgerc')
        if is_file == True:
            return True
        else:
            print 'Credentials file (.edgerc) not located in project directory'
            return False

    except:
        print 'Project path not collected'

def check_for_env_vars():
    """ Check for environment variables for LDS API credentials; return with __with_environment() function """
    try:
        config = {}
        config['EDGE_BASE_URL'] = os.environment.get('EDGE_BASE_URL')
        config['EDGE_CLIENT_TOKEN'] = os.environ.get('EDGE_CLIENT_TOKEN')
        config['EDGE_CLIENT_SECRET'] = os.environ.get('EDGE_CLIENT_SECRET')
        config['EDGE_ACCESS_TOKEN'] = os.environ.get('EDGE_ACCESS_TOKEN')

        for key in config.items():
            if key == None:
                print '%s value was not found' % key
                return False
            else:
                print '%s value found' % key

        return True
    except:
        raise BaseException('Unable to access environment variables')
