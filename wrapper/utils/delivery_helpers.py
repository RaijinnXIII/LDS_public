

def delivery_type_validation(entry):
    valid_types = ['email', 'ftp']

    if not entry in valid_types:
        raise ValueError('Invalid delivery_type value')

    return True
