

def source_type_validation(entry):
    valid_types = ['answerx-objects', 'cpcode-products', \
        'edns-zones', 'gtm-properties', 'gtm', 'edns', 'answerx', 'etp']

    if not entry in valid_types:
        raise ValueError('Invalid log source type')
    elif entry is None:
        raise ValueError('Log source type is required')
    else:
        return True


def source_id_validation(entry):
    if not isinstance(entry, str):
        raise TypeError('Entry must be a string')

    return True
