from authentication import Authentication
from utils.source_helpers import source_type_validation


class LogSources:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-sources'
            self.url = self.client.BASE_URL + self.endpoint

    def list(self):
        """
        Usage: List all available Log Sources

        Returns HTTP response
        """
        return self.client.session.get(self.url)

    def list_by_type(self, log_source_type):
        """
        Usage: Lists all Log Sources by type (e.g. cpcode-products)

        Param: log_source_type (str) - one of predefined set of strings

        Returns HTTP response
        """
        if source_type_validation(log_source_type) == True:
            return self.client.session.get(self.url)

    def retrieve(self, log_source_type, source_id):
        """
        Usage: Retrieve Log Source by Type and ID

        Params:
        log_source_type (str)
        source_id (str)

        Returns HTTP response
        """
        if source_type_validation(log_source_type) == True:
            url = self.url \
                + '/%s' % str(source_id)
            return self.client.session.get(url)


# CLIENT = Authentication()
# print vars(CLIENT)
# LOG_SOURCES = LogSources(CLIENT)
# print LOG_SOURCES.list().status_code
# print LOG_SOURCES.list_by_type('cpcode-products').status_code
