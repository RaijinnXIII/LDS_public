from authentication import Authentication

class LogDeliveryFrequencies:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-configuration-parameters/delivery-frequencies'
            self.url = self.client.BASE_URL + self.endpoint

    def list(self):
        return self.client.session.get(self.url)

    def retrieve(self, frequency_id):
        if frequency_id == None:
            raise ValueError('frequency_id is required')

        url = self.url + '/%s' % str(frequency_id)
        return self.client.session.get(url)
