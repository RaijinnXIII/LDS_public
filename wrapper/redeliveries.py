from authentication import Authentication
import json


class LogRedeliveries:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-redeliveries'
            self.url = self.client.BASE_URL + self.endpoint

    def list(self):
        """
        Usage: List all Log Redeliveries

        No params

        Returns HTTP response
        """
        return self.client.session.get(self.url)


    def retrieve(self, redelivery_id):
        """
        Usage: Retrieve specific Log Redelivery

        Params:
        redelivery_id (str)

        Returns HTTP response
        """
        if redelivery_id == None or redelivery_id == '':
            raise ValueError('Redelivery ID cannot be empty')

        url = self.url + '/%s' % str(redelivery_id)
        return self.client.session.get(url)

    def create(self):
        """
        Usage: Create new Log Redelivery

        Params:
        JSON object w/ required fields

        Returns HTTP response
        """
        pass
