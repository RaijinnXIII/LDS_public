from authentication import Authentication
from utils.source_helpers import source_type_validation, source_id_validation


class LogSourceConfigurations:
    def __init__(self, CLIENT=None):
        if CLIENT is None:
            raise ValueError('Auth instance required')
        else:
            self.client = CLIENT
            self.endpoint = '/lds-api/v3/log-sources'
            self.url = self.client.BASE_URL + self.endpoint

    def list_by_type(self, log_source_type):
        if source_type_validation(log_source_type) == True:
            url = self.url + '/%s/log-configurations' % str(log_source_type)
            return self.client.session.get(url)

    def list_by_id(self, log_source_type, log_source_id):
        if source_type_validation(log_source_type) == True:
            if source_id_validation(log_source_id) == True:
                url = self.url + '/%s/%s/log-configurations' % \
                    (str(log_source_type), str(log_source_id))
                return self.client.session.get(url)

    def retrieve(self, log_source_type, log_source_id):
        if source_type_validation(log_source_type) == True:
            if source_id_validation(log_source_id) == True:
                url = self.url + '/%s/%s/log-configurations' % \
                    (str(log_source_type), str(log_source_id)
                return self.client.session.get(url)

    def create(self, log_source_type, log_source_id):
        data = {} # Needs JSON schema
        if source_type_validation(log_source_type) == True:
            if source_id_validation(log_source_id) == True:
                url = self.url + '/%s/%s/log-configurations' % \
                    (str(log_source_type), str(log_source_id))
                return self.client.session.post(url, payload=data)

    def update(self, log_source_type, log_source_id):
        data = {} # Needs JSON schema
        if source_type_validation(log_source_type) == True:
            if source_id_validation(log_source_id) == True:
                url = self.url + '/%s/%s/log-configurations' % \
                    (str(log_source_type), str(log_source_id))
                return self.client.session.post(url, payload=data)

    def suspend(self, log_source_type, log_source_id):
        if source_type_validation(log_source_type) == True:
            if source_id_validation(log_source_id) == True:
                url = self.url + '/%s/%s/log-configurations/suspend' % \
                    (str(log_source_type), str(log_source_id))
                return self.client.session.post(url)

    def resume(self, log_source_type, log_source_id):
        if source_type_validation(log_source_type) == True:
            if source_id_validation(log_source_id) == True:
                url = self.url + '/%s/%s/log-configurations/resume' % \
                    (str(log_source_type), str(log_source_id)
                return self.client.session.post(url)
