## Client & Wrapper for the Akamai LDS (v3) API ##

**Objectives of Project**
1. Include a plug-and-play client for quick & easy session authentication
2. Provide a flexible wrapper package for integrated or standalone use

**Priorities**
1. Completeness
2. Reliability
3. Ease of Use

**Roadmap**
1. Setup (complete)
2. Authentication & Session (complete)
3. Wrapper methods (complete)
4. Tests for Auth/Session
5. Tests for Wrapper methods
6. Global linting
7. PIP package registration
8. Update usage docs
9. Share project


**Future 'Far' Away**
- CLI tool
- response format options
  (not just HTTP object, but also json and byte string format)

----

### Features: ###

- **HTTPS only**: Insecure connections not supported
- **Dynamic Credential Retrieval**: Checks for file, then environment, for credentials during **Authorization()**
- **Unit & Integration Tests**: Proves installation is setup and operational
- **Available in PIP**: Install the package easily with `pip install lds_public`

### Usage: ###
N/A
